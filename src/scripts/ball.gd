extends KinematicBody2D

var speed = 100.0
var direction
var colliding = false

func _ready():
	
	var start_angle = 1.05 + randf() * 0.9
	
	direction = Vector2( cos(start_angle), sin(start_angle) )
	
	set_fixed_process( true )

func _fixed_process(delta):
	
	if get_parent().is_playing() == true :
		if is_colliding() and colliding == false:
			colliding = true
			
			speed += 1
			
			var normal = get_collision_normal()
			direction = normal.reflect(direction)
			
			if get_collider() in get_tree().get_nodes_in_group("Bubles") :
				get_node("Sound_Effects").play("pick")
				get_collider().free()
			elif get_collider().get_name() == "Player" :
				get_node("Sound_Effects").play("jump")
			else:
				get_node("Sound_Effects").play("reflect")
		else:
			if is_colliding():
				var normal = get_collision_normal()
				direction = normal.normalized()
		
		if is_colliding() == false and colliding == true :
			colliding = false
			
		var ds = delta * speed
		var pohyb = Vector2 ( ds * direction.x , ds * direction.y)
		move( pohyb )