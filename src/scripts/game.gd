
extends Node

var playing

func _ready():
	playing = false
	get_node("Game_Status").set_opacity(1)
	set_fixed_process(true)

func _fixed_process(delta):
	
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	
	if Input.is_action_pressed("ui_accept") and playing == false :
		playing = true
		get_node("Game_Status").set_opacity(0)
	
	if playing :
		
		if get_node("Ball").get_pos().y > (get_viewport().get_size_override().height+32) :
			playing = false
			get_node("/root/scene_loader").goto_scene("scenes/game.scn")

func is_playing():
	return playing