
extends KinematicBody2D

const SPEED = 200 # constantna rychlost
var direction = 0 # urcuje smer : -1 Left, 0 nothing, 1 Right
var start_pos = get_pos()

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	
	#if get_tree().get_root().is_playing() :
	if get_parent().is_playing():
		if Input.is_action_pressed("ui_left"):
			direction = -1
		elif Input.is_action_pressed("ui_right"):
			direction = 1
		else:
			direction = 0
			
		var ds = SPEED * delta
		var pohyb = Vector2(direction * ds,0)
		
		move( pohyb )
		set_pos( Vector2( get_pos().x, start_pos.y ))
	else:
		pass