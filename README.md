# **Bubles Break** #
Small simple pong-style game. Created in [Godot 2](http://www.godotengine.org/projects/godot-engine). I created this game as example for my tutorials.

## **Downloads** ##
* [Windows](https://bitbucket.org/CDmir230/bublesbreak/downloads/bublesbreak_win.zip)
* [Mac OSX](https://bitbucket.org/CDmir230/bublesbreak/downloads/bublesbreak_osx.zip)
* [Linux](https://bitbucket.org/CDmir230/bublesbreak/downloads/bublesbreak_linux.7z)

## **Build From Source Codes** ##
* Download [Godot (>=2.0)](http://www.godotengine.org/projects/godot-engine)
* Download Source Code from [Download](https://bitbucket.org/CDmir230/bublesbreak/downloads) 
* Run Godot
* Push Import Button
* Set path to '**your_path/BublesBreak/src/engine.cfg**'
* Run or Edit